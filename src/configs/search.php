<?php
return array(
	'sites' => array(
		'www' => array(
			'google' => array(
				'.' => 'www.google'
			),
			'bing' => array(
				'com' => array(
					'.' => 'www.bing.com'
				)
			)
		),
		'bing' => array(
			'com' => array(
				'.' => 'www.bing.com'
			)
		),
		'search' => array(
			'yahoo' => array(
				'com' => array(
					'.' => 'search.yahoo.com'
				)
			)
		),
		'cse' => array(
			'google' => array(
				'.' => 'cse.google.com'
			)
		),
		'coccoc' => array(
			'com' => array(
				'.' => 'coccoc.com'
			)
		),
		'newtab' => array(
			'club' => array(
				'.' => 'newtab.club'
			)
		),
		'busca' => array(
			'uol' => array(
				'com' => array(
					'.' => 'busca.uol.com'
				)
			)
		)
	)
);
?>