<?php
if (empty($_SERVER['PATH_INFO'])) {
	exit;
}


$class_name = substr($_SERVER['PATH_INFO'], 1);


CONST MODULE_PATH = 'modules/';
CONST ABSTRACT_PATH = 'abstract/';
CONST PHP_SUFFIX = '.php';


spl_autoload_register(function($class) {
	$file_name = $class . PHP_SUFFIX;

	if (is_file($file = (MODULE_PATH . $file_name)) || is_file($file = (ABSTRACT_PATH . $file_name))) {
		include_once($file);
	} else {
		exit;
	}
});



$class = ucfirst($class_name);

new $class;
?>