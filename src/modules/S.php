<?php
class S extends ConfigLoaderAbstract {

	CONST CB = 'cb';
	CONST KEYWORD = '{KEYWORD}';

	private static $CONFIG = array(array(
		'template' => array(
			'title' => '<span style="text-transform:capitalize;">{KEYWORD}</span> - <span style="text-transform:capitalize;">{KEYWORD}</span>.com',
			'url' => '{KEYWORD}.com/{KEYWORD}',
			'desc' => 'Now get {KEYWORD} Here. Search For Best {KEYWORD} Content!<br/>Get Info Right Now · Find What You Search For · All About Results · Save Time and Money'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => 'Top Rated <span style="text-transform:capitalize;">{KEYWORD}</span> | <span style="text-transform:capitalize;">{KEYWORD}</span>.com',
			'url' => 'Top{KEYWORD}.com/{KEYWORD}',
			'desc' => 'Get the top information of {KEYWORD} all over the world from Internet. Check out right now.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => 'Find the best deals for <span style="text-transform:capitalize;">{KEYWORD}</span>',
			'url' => 'Find{KEYWORD}/{KEYWORD}',
			'desc' => 'Great savings on many {KEYWORD}. Check out the offers with lowest price.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => 'The Best Free <span style="text-transform:capitalize;">{KEYWORD}</span> Sites',
			'url' => 'Freesearch/{KEYWORD}',
			'desc' => 'Get the best free services for {KEYWORD} right now. Get all you want here.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => '<span style="text-transform:capitalize;">{KEYWORD}</span> - Updated Reviews|Search for <span style="text-transform:capitalize;">{KEYWORD}</span>',
			'url' => '{KEYWORD}/Results',
			'desc' => 'Search for {KEYWORD}. Browse it now!'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => '<span style="text-transform:capitalize;">{KEYWORD}</span> - Information for you | search<span style="text-transform:capitalize;">{KEYWORD}</span>.com',
			'url' => 'search{KEYWORD}.com/Results',
			'desc' => 'Search for {KEYWORD} in the world.Get the info right now.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => '<span style="text-transform:capitalize;">{KEYWORD}</span> Home - Search <span style="text-transform:capitalize;">{KEYWORD}</span> Home',
			'url' => 'www.excedese.com/{KEYWORD}/Now',
			'desc' => 'Search {KEYWORD} home. Get combined results from popular engines at once.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => 'Free <span style="text-transform:capitalize;">{KEYWORD}</span> - Look what we have got for <span style="text-transform:capitalize;">{KEYWORD}</span>',
			'url' => 'free{KEYWORD}/Reults/',
			'desc' => 'Check out free {KEYWORD} from Internet worldwide.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => 'Great Prices & Deals on <span style="text-transform:capitalize;">{KEYWORD}</span>',
			'url' => '{KEYWORD}.com',
			'desc' => 'Low prices on many {KEYWORD}, and Great savings on many {KEYWORD}.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => 'Search Top Free Info on <span style="text-transform:capitalize;">{KEYWORD}</span> | Top-<span style="text-transform:capitalize;">{KEYWORD}</span>',
			'url' => 'Top-{KEYWORD}.com/Results',
			'desc' => 'Find what you need when you need about {KEYWORD}. Click Here to Get the top info for {KEYWORD}'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => '<span style="text-transform:capitalize;">{KEYWORD}</span> - Compare <span style="text-transform:capitalize;">{KEYWORD}</span> Quotes and Save',
			'url' => '{KEYWORD}.com/compare',
			'desc' => 'Compare {KEYWORD} quotes and find all the information you need to save on your {KEYWORD} policy. Get the info you need, quick, easy and free.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => '70% Off For <span style="text-transform:capitalize;">{KEYWORD}</span> Shopping Online',
			'url' => 'shopping.online/discount/{KEYWORD}',
			'desc' => 'Search and Get your best discount for {KEYWORD} shopping online today. Click here to see how much you can save today.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => 'Find more about <span style="text-transform:capitalize;">{KEYWORD}</span>',
			'url' => 'Find/{KEYWORD}',
			'desc' => 'Learn more about {KEYWORD} from the largest collections online. Just click to Get it quick, easy and free.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => '<span style="text-transform:capitalize;">{KEYWORD}</span> - Get Official Site',
			'url' => '{KEYWORD}/official',
			'desc' => 'Check out official site of {KEYWORD} online quickly. All you need just in one place.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => '<span style="text-transform:capitalize;">{KEYWORD}</span> online - Get and Play <span style="text-transform:capitalize;">{KEYWORD}</span>',
			'url' => '{KEYWORD}.com',
			'desc' => 'Search and Step into the fantastic {KEYWORD} world online. And Get it for you totally free today.'
		),
		'click' => 'http://ck.excedese.com?q='
	));

	private $results = array();


	public function exec() {
		$this->setParam();

		if (empty($_REQUEST['sub1']) || empty($_REQUEST['q'])) {
			exit;
		}

		$kwd = trim(strip_tags($_REQUEST['q']));
		$guid = '';

		if (empty($kwd)) {
			exit;
		}

		if (!empty($_REQUEST['guid'])) {
			$guid = $_REQUEST['guid'];
		}

		$sub1 = $_REQUEST['sub1'];
		$rc = empty($_REQUEST['rc']) ? 3 : ($_REQUEST['rc'] | 0);

		$lower_kwd = strtolower($kwd);
		$encode_kwd =urlencode($kwd);
		$url_kwd = join('-', explode(' ', $lower_kwd));
		$KEYWORD = self::KEYWORD;
		$template_configs = self::$CONFIG;
		$param = $encode_kwd . '&gid=' . $guid . '&at=1' . '&sub1=' . $sub1 . '&uid=' . (isset($_REQUEST['uid']) ? $_REQUEST['uid'] : '');

		$this->getLanguageConfig($this->cc, $languageTemplates);

		if (is_array($languageTemplates)) {
			$this->format($KEYWORD, $lower_kwd, $url_kwd, $param, $languageTemplates[rand(0, count($languageTemplates) - 1)]);
		}
		
		for ($i = 0; $i < $rc; $i++) {
			$rand = rand(0, count($template_configs) - 1);
			$ad = $template_configs[$rand];
			
			$this->format($KEYWORD, $lower_kwd, $url_kwd, $param, $ad);

			if (1 === count($template_configs)) {
				$template_configs = self::$CONFIG;
			} else {
				array_splice($template_configs, $rand, 1);
			}
		}
		

		$this->printResults($this->results);
	}


	private function format(&$KEYWORD, &$lower_kwd, &$url_kwd, &$param, &$ad) {
		$template = &$ad['template'];

		$this->results[] = array(
			'template' => array(
				'title' => str_replace($KEYWORD, $lower_kwd, $template['title']),
				'url' => str_replace($KEYWORD, $url_kwd, $template['url']),
				'desc' => str_replace($KEYWORD, $lower_kwd, $template['desc'])
			),
			'click' => $ad['click'] . $param
		);
	}


	private function setParam() {
		$_REQUEST['et'] = 2;
		$_REQUEST['at'] = 1;
	}
}
?>