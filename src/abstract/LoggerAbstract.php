<?php
require __dir__ . '/../geo/vendor/autoload.php';

abstract class LoggerAbstract {

	CONST APACHE_KEY = 'info';
	CONST SPLIT = '[:**]';

	private static $PARAM_LIST = array(
		'et',
		'at',
		'guid',
		'uid',
		'sub1',
		'q',
		'ua',
		'ref',
		'n',
		'p',
		'ip'
	);

	protected $ip;


	function __construct() {
		$gi = geoip_open(__dir__ . '/../geo/GeoIP.dat', GEOIP_STANDARD);
		$this->ip = $this->get_ip();
		$_REQUEST['ip'] = $this->ip;
		$this->cc = geoip_country_code_by_addr($gi, $this->ip);

		if (!$this->cc) {
			$this->cc = 'CN';
		}

		$this->init();
	}


	abstract public function init();



	function __destruct() {
		$log_params = array();

		foreach (self::$PARAM_LIST as $parame) {
			/*if (isset($_REQUEST[$parame])) {
				$log_params[] = $_REQUEST[$parame];
			}*/
			$log_params[] = isset($_REQUEST[$parame]) ? $_REQUEST[$parame] : '';
		}
		
		apache_note(self::APACHE_KEY, join(self::SPLIT, $log_params));
	}


	private final function get_ip() {
		$ipArr = $this->get_user_ip();
		$IpExArr = explode(',', $ipArr);

		if (is_array($IpExArr)) {
			$IpExArr = array_reverse($IpExArr);
			$IPLength = count($IpExArr);

			foreach ($IpExArr as $ip) {
				if (empty($ip)) continue;

				$iplist = explode('.', $ip);
				if ($iplist[0] == '10' && $IPLength > 1) continue;

				$lastIp = $ip;
				break;
			}

			if (empty($lastIp))
				$lastIp = $IpExArr[0];

		} else {
			$lastIp = $ipArr;
		}

		return trim($lastIp);
	}


	private function get_user_ip() {
		if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
			$IpIn = $_SERVER['HTTP_CF_CONNECTING_IP'];
			return $IpIn;
		}
		if (isset($HTTP_SERVER_VARS['HTTP_CLIENT_IP'])) { 
			$IpIn = $HTTP_SERVER_VARS['HTTP_CLIENT_IP']; 
		} elseif (isset($HTTP_SERVER_VARS['HTTP_X_FORWARDED_FOR'])) { 
			$IpIn = $HTTP_SERVER_VARS['HTTP_X_FORWARDED_FOR']; 
		} elseif (isset($HTTP_SERVER_VARS['REMOTE_ADDR'])) { 
			$IpIn = $HTTP_SERVER_VARS['REMOTE_ADDR']; 
		} elseif (getenv('HTTP_CLIENT_IP')) { 
			$IpIn = getenv('HTTP_CLIENT_IP'); 
		} elseif (getenv('HTTP_X_FORWARDED_FOR')) { 
			$IpIn = getenv('HTTP_X_FORWARDED_FOR'); 
		} elseif (getenv('REMOTE_ADDR')) { 
			$IpIn = getenv('REMOTE_ADDR'); 
		} else { 
			$IpIn = '127.0.0.1'; 
		} 
		//$IpIn = "24.131.207.228";
		return $IpIn;
	}
}
?>