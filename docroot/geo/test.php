<?php


require 'vendor/autoload.php';

$gi = geoip_open("./GeoIP.dat",GEOIP_STANDARD);

echo geoip_country_code_by_addr($gi, "24.24.24.24") . "\t" .
     geoip_country_name_by_addr($gi, "24.24.24.24") . "\n";
echo geoip_country_code_by_addr($gi, "80.24.24.24") . "\t" .
     geoip_country_name_by_addr($gi, "80.24.24.24") . "\n";
echo geoip_country_code_by_addr($gi, "104.224.144.68") . "\t" .
     geoip_country_name_by_addr($gi, "104.224.144.68") . "\n";
geoip_close($gi);

