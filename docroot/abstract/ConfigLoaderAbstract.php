<?php
abstract class ConfigLoaderAbstract extends LoggerAbstract {

	CONST CB = 'cb';
	CONST CONFIG_PATH = '/../configs/config.php';

	private $config;


	abstract public function exec();


	public final function init() {
		$this->config = include __dir__ . self::CONFIG_PATH;
		$this->exec();
	}


	public final function getConfig($key, &$results) {
		$results = include $this->config[$key];
	}


	public final function getSiteConfig($key, &$results) {
		if (is_file($file = ($this->config['siteConfig'] . $key . '.php'))) {
			$results = include $file;
		}
	}


	public final function getLanguageConfig($key, &$results) {
		if (is_file($file = ($this->config['language'] . $key . '.php'))) {
			$results = include $file;
		}
	}


	public final function getIntextConfig($key, &$results) {
		if (is_file($file = ($this->config['intext'] . $key . '.php'))) {
			$results = include $file;
		}
	}


	public final function printResults(&$results) {
		$results_json = json_encode($results);
		echo empty($_REQUEST[self::CB]) ? $results_json : ($_REQUEST[self::CB] . '(' . $results_json . ');');
	}
}
?>