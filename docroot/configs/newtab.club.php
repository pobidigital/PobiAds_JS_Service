<?php
$item = <<<ITEM
<div style="padding:10px 0 10px 0;">
	<div>
		<a a-link a-title style="line-height:1.4em;font-size:18px;color:#00c"></a>
	</div>
	<div style="color:#008000;">
		<span style="display:inline-block;margin-right:7px;padding:0 3px 0 2px;line-height:12px;border:1px solid #008000;border-radius:3px;font-size:12px;vertical-align:baseline;">Ad</span>
		<a a-link a-url style="font-size:14px;text-decoration:none;color:inherit!important"></a>
    </div>
    <span a-desc style="font-size:13px;color:#545454;"></span>
</div>
ITEM;


return  array(
	// 'dir' => array(
	// 	'type' => 0,	//0:form.action
	// 	'selector' => '#search_form [name="q"], .gsc-search-box.gsc-search-box-tools #gsc-i-id1',
	// 	'sugselector' => 'body > .gstl_50.gssb_c',
	// 	'attr' => 'query',
	// 	'target' => '_blank'
	// ),
	'slots' => array(
		't' => array(
			'selector' => '.gsc-wrapper',
			'type' => 0,
			'template' => array(
				'Serp' => array(
					'rc' => 3,
					'wrapper' => '<div style="padding:4px;border:1px solid #fff;"><div item style="padding:2px 0;margin:0;word-wrap:break-word;"></div></div>',
					'item' => $item
				)
			)
		),
		'b' => array(
			'selector' => '.gsc-cursor-box.gs-bidi-start-align',
			'type' => 0, //0: beforeBegin, 1: afterBegin, 2: beforeEnd, 3: afterEnd
			'template' => array(
				'Serp' => array(
					'rc' => 2,
					'wrapper' => '<div style="padding:4px;border:1px solid #fff;"><div item style="padding:2px 0;margin:0;word-wrap:break-word;"></div></div>',
					'item' => $item
				)
			)
		)
	),
	'query' => array(
		'name' => '#search_form [name="q"], .gsc-search-box.gsc-search-box-tools #gsc-i-id1',
		'type' => 1, //0: url parse, 1: ele. 2: ele.getAttribute
		'prop' => 'value'
	)
);
?>