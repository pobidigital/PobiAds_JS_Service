<?php
$t_item = <<<ITEM
<div class="_2NV6E ads _2J2eG">
	<div class="_2YzXf">
		<ul class="_2vSRx">
			<li class="_3ajiF">
				<div>
					<h3>
						<a target="_blank" a-link><span a-title></span></a>
					</h3>
					<p class="_19iPC">
						<a class="_1pQnS" target="_blank" a-link><span class="uljSk"></span><span a-url></span></a>
					</p>
				</div>

				<div class="hWPex">
					<p a-desc></p>
				</div>
			</li>
		</ul>
	</div>
</div>
ITEM;


return  array(
	'dir' => array(
		'type' => 0,	//0:form.action
		'selector' => '#sb_form [name="q"]',
		'sugselector' => '#sw_as .sa_drw',
		'attr' => 'query'
	),
	'slots' => array(
		't' => array(
			'selector' => '._2FHYw:nth-child(2)',
			'type' => 0,
			'template' => array(
				'Serp' => array(
					'rc' => 3,
					'wrapper' => '<div class="_2FHYw"><style>.algocore .b_algo{box-sizing:border-box;margin:0 16px 10px 0!important;padding:10px 10px 10px 20px!important;}</style><div item></div></div>',
					'item' => $t_item
				)
			)
		),
		'b' => array(
			'selector' => '._2LTzu',
			'type' => 3, //0: beforeBegin, 1: afterBegin, 2: beforeEnd, 3: afterEnd
			'template' => array(
				'Serp' => array(
					'rc' => 2,
					'wrapper' => '<div><div item></div></div>',
					'item' => $t_item
				),
				'GRS' => array(
					'selector' => '._2x-pA a',
					'term' => '[\?&#]query=([^&]+)',
					'direct' => 'http://ck.excedese.com/web?qs={KWD}',
				)
			)
		)
	),
	'query' => array(
		'name' => 'input._3tlk0',
		'type' => 1, //0: url parse, 1: ele. 2: ele.getAttribute
		'prop' => 'value'
	)
);
?>