<?php
$item = <<<ITEM
<div style="padding:2px 0;margin:0;word-wrap:break-word;font-size:12px;">
	<div style="position:relative;-ms-flex-direction:column;-webkit-box-orient:vertical;-webkit-flex-direction:column;flex-direction:column;-ms-flex-align:start;-webkit-box-align:start;-webkit-align-items:flex-start;align-items:flex-start;-ms-flex-pack:start;-webkit-box-pack:start;-webkit-justify-content:flex-start;justify-content:flex-start;padding:4px;">
		<div>
			<div>
				<div>
					<a a-link a-title style="line-height:1.4em;margin:0;padding:0;font-size:16px;font-weight:normal;color:#00c;"></a>
				</div>
				<span style="display:inline-block;margin-right:7px;padding:0 3px 0 2px;line-height:11px;font-size:11px;border-radius:3px;border:1px solid #008000;color:#008000;vertical-align:baseline;">Ad</span>
				<a a-link a-url style="font-size:13px;font-style:normal;font-weight:normal;color:#008000;text-decoration:none;"></a>
				<br/>
				<span a-desc style="font-size:13px;"></span>
			</div>
		</div>
		<div style="display:-ms-flexbox;display:-webkit-box;display:-webkit-flex;display:flex;box-sizing:border-box;-ms-flex-direction:row;-webkit-box-orient:horizontal;-webkit-flex-direction:row;flex-direction:row;-ms-flex-align:start;-webkit-box-align:start;-webkit-align-items:flex-start;align-items:flex-start;-ms-flex-pack:start;-webkit-box-pack:start;-webkit-justify-content:flex-start;justify-content:flex-start;">
			<a a-link style="-ms-flex-pack:space-around;-webkit-box-pack:justify;-webkit-justify-content:space-around;justify-content:space-around;-ms-flex-align:center;-webkit-box-align:center;-webkit-align-items:center;align-items:center;margin-top:4px;max-width:200px;height:28px;cursor:pointer;margin-bottom:7px;padding-left:5px;padding-right:5px;border-radius:2px;text-decoration:none;font-size:13px;line-height:1.5em;color:#0000cc;font-weight:normal;text-decoration:none;vertical-align:bottom;box-shadow:0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);transition:all .08s linear,min-width .15s cubic-bezier(0.4,0.0,0.2,1);box-sizing:border-box;display:flex;display:-ms-flexbox;display:-webkit-box;display:-webkit-flex;position:relative;margin-left:1px;">
				<svg style="width:13px;height:13px;padding-right:3px;" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg" class="arrowSvgButton">
					<defs>
						<polygon id="path-1" points="10 3.33333333 8.825 4.50833333 13.475 9.16666667 3.33333333 9.16666667 3.33333333 10.8333333 13.475 10.8333333 8.825 15.4916667 10 16.6666667 16.6666667 10" fill="#00c"></polygon>
					</defs>
					<g id="V1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-1196.000000, -367.000000)">
						<g id="Concept-1" transform="translate(53.000000, 53.000000)">
							<g id="Spec" transform="translate(704.000000, 68.000000)">
								<g id="Navigation/ic_arrow_forward" transform="translate(436.000000, 243.000000)">
									<polygon id="Bounds" points="0 0 20 0 20 20 0 20"></polygon>
									<mask id="mask-2">
										<use xlink:href="#path-1"></use>
									</mask>
									<use id="Mask" class="jb_" fill-rule="evenodd" xlink:href="#path-1"></use>
									<g id="Color-/-Color-08" mask="url(#mask-2)" fill-rule="evenodd">
										<rect id="color" x="0" y="0" width="20" height="20"></rect>
									</g>
								</g>
							</g>
						</g>
					</g>
				</svg>
				<div style="max-width:100%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;">
					<span> Visit Website </span>
				</div>
			</a>
		</div>
	</div>
</div>
ITEM;


return  array(
	'slots' => array(
		't' => array(
			'selector' => '.gsc-wrapper',
			'type' => 1,
			'template' => array(
				'Serp' => array(
					'rc' => 3,
					'wrapper' => '<div><div item></div></div>',
					'item' => $item
				)
			)
		),
		'b' => array(
			'selector' => '.gsc-cursor-box.gs-bidi-start-align',
			'type' => 0, //0: beforeBegin, 1: afterBegin, 2: beforeEnd, 3: afterEnd
			'template' => array(
				'Serp' => array(
					'rc' => 2,
					'wrapper' => '<div><div item></div></div>',
					'item' => $item
				),
				'GRS' => array(
					'selector' => '#brs a',
					'term' => '[\?&]q=([^&]+)',
					'direct' => 'http://ck.excedese.com?qs={KWD}',
				)
			)
		)
	),
	'query' => array(
		'name' => '[name="search"]',
		'type' => 1, //0: url parse, 1: ele. 2: ele.getAttribute
		'prop' => 'value',
		'sListener' => '.gsc-resultsbox-visible'
	),
	'tag' => 1
);
?>