<?php
$item = <<<ITEM
<article class="result-item">
	<h1>
		<a a-link a-title></a>
	</h1>
	<div class="url"><span style="display:inline-block;margin-right:7px;padding:0 3px 0 2px;border:1px solid #080;border-radius:3px;font-size:13px;vertical-align:baseline;">Anúncio</span><span a-url></span></div>
	<div class="description" a-desc></div>
</article>
ITEM;


return  array(
	'slots' => array(
		't' => array(
			'selector' => '#result > section:nth-child(2) > .font-zero',
			'type' => 3,
			'template' => array(
				'Serp' => array(
					'rc' => 3,
					'wrapper' => '<div class="padding:4px;" item></div>',
					'item' => $item
				)
			)
		),
		'b' => array(
			'selector' => '.result-list + section > .clear',
			'type' => 0, //0: beforeBegin, 1: afterBegin, 2: beforeEnd, 3: afterEnd
			'template' => array(
				'Serp' => array(
					'rc' => 2,
					'wrapper' => '<div><div item></div></div>',
					'item' => $item
				),
				'GRS' => array(
					'selector' => '.b_rs a',
					'term' => '[\?&]q=([^&]+)',
					'direct' => 'http://ck.excedese.com/web?qs={KWD}',
				)
			)
		)
	),
	'query' => array(
		'name' => '#term',
		'type' => 1, //0: url parse, 1: ele. 2: ele.getAttribute
		'prop' => 'value'
	)
);
?>