<?php
return array(
	'monetisations' => __dir__ . '/monetisations.php',
	'search' => __dir__ . '/search.php',
	'siteConfig' => __dir__ . '/',
	'language' => __dir__ . '/language/',
	'intextSites' => __dir__ . '/intext/sites.php',
	'intext' => __dir__ . '/intext/',
	'keywords' => __dir__ . '/hot_keywords.php'
);
?>