<?php
return  array(
	'slots' => array(
		't' => array(
			'selector' => '#center_col',
			'type' => 1,
			'template' => array(
				'Serp' => array(
					'rc' => 3,
					'wrapper' => '<section style="position:relative;margin-bottom:27px;line-height:22px;color:#4d5156;"><section item></section></section>',
					'item' => join('', array(
						'<div style="position:relative;margin-bottom:28px;">',
							'<a a-link style="text-decoration:none;">',
								'<br />',
								'<h3 a-title style="font-size:20px;display:inline-block;line-height:26px;margin-bottom:3px;padding-top:3px;"></h3>',
								'<div style="position:absolute;font-size:14px;display:inline-flex;align-items:baseline;max-width:600px;top:0;left:0;line-height:1.5;padding:1px 0 3px;white-space:nowrap;color:#202124;">',
									'<span style="display:inline-block;margin:0;padding:0;font-weight:bold;background-color:#fff;">Ad<span style="padding:0 5px;">·</span></span>',
									'<cite a-url></cite>',
								'</div>',
							'</a>',
							'<div style="position:absolute;font-size:14px;display:inline-flex;align-items:baseline;top:0;left:0;max-width:600px;height:0;line-height:1.5;padding:1px 0 3px;white-space:nowrap;color:#202124;">',
								'<span style="display:inline-block;margin:0;padding:0;font-weight:bold;background-color:#fff;visibility:hidden;">Ad<span style="padding:0 5px">·</span></span>',
								'<cite a-url style="visibility:hidden;"></cite>',
								'<div class="action-menu ab_ctl">',
									'<a class="GHDvEf ab_button" href="#" aria-label="Result details" aria-expanded="false" aria-haspopup="true" role="button" jsaction="m.tdd;keydown:m.hbke;keypress:m.mskpe" data-ved="0ahUKEwj-joLa8LbaAhVIspQKHW5GAPgQ7B0IJjAA">',
										'<span class="mn-dwn-arw"></span>',
									'</a>',
									'<div class="action-menu-panel ab_dropdown" role="menu" tabindex="-1" jsaction="keydown:m.hdke;mouseover:m.hdhne;mouseout:m.hdhue" data-ved="0ahUKEwj-joLa8LbaAhVIspQKHW5GAPgQqR8IJzAA">',
										'<ol>',
											'<li class="action-menu-item ab_dropdownitem" role="menuitem">',
												'<a class="fl" a-link style="font-size:14px;line-height:22px;padding:10px 16px;"><span class="c0Xbsc z1asCe A2gxLb"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11 18h2v-2h-2v2zm1-16C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm0-14c-2.21 0-4 1.79-4 4h2c0-1.1.9-2 2-2s2 .9 2 2c0 2-3 1.75-3 5h2c0-2.25 3-2.5 3-5 0-2.21-1.79-4-4-4z"></path></svg></span>Why this ad?</a>',
											'</li>',
											'<li class="action-menu-item ab_dropdownitem" role="menuitem">',
												'<a class="fl" a-link style="font-size:14px;line-height:22px;padding:10px 16px;"><span class="c0Xbsc z1asCe A2gxLb"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11 18h2v-2h-2v2zm1-16C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm0-14c-2.21 0-4 1.79-4 4h2c0-1.1.9-2 2-2s2 .9 2 2c0 2-3 1.75-3 5h2c0-2.25 3-2.5 3-5 0-2.21-1.79-4-4-4z"></path></svg></span>About the advertiser</a>',
											'</li>',
										'</ol>',
									'</div>',
								'</div>',
							'</div>',
							'<div a-desc style="line-height:22px;color:#4d5156;"></div>',
							'<style>.ab_ctl.action-menu{margin-top:1px;vertical-align:middle;}.action-menu{display:inline;margin:0 3px;position:relative;-webkit-user-select:none;}.f,.f a:link{color:#808080;}a.ab_button,a.ab_button:visited{display:inline-block;margin-top:1px;}a.ab_button{text-decoration:none;cursor:default;}.GHDvEf, .GHDvEf:hover, .GHDvEf.selected, .GHDvEf.selected:hover{background-color:white;background-image:none;border:0;border-radius:0;box-shadow:0 0 0 0;cursor:pointer;filter:none;height:12px;min-width:0;padding:0;transition:none;-webkit-user-select:none;width:13px;}.action-menu .mn-dwn-arw {border-color:#70757a transparent;margin-top:-4px;margin-left:3px;left:0;}.mn-dwn-arw{border-style:solid;border-width:5px 4px 0 4px;width:0;height:0;top:50%;position:absolute;}.action-menu-panel{left:0;padding:0;right:auto;top:12px;visibility:hidden;}.ab_dropdown {background:#fff;border:1px solid rgba(0,0,0,0.2);font-size:13px;position:absolute;top:32px;white-space:nowrap;z-index:3;-webkit-transition:opacity 0.218s;transition:opacity 0.218s;-webkit-box-shadow:0 2px 4px rgba(0,0,0,0.2);box-shadow:0 2px 4px rgba(0,0,0,0.2);}.action-menu-button, .action-menu-item a.fl, .action-menu-toggled-item div{color:#333;display:block;padding:7px 18px;text-decoration:none;outline:0;}</style>',
						'</div>'
					))
				)
			)
		),
		'b' => array(
			'selector' => '#brs',
			'type' => 0, //0: beforeBegin, 1: afterBegin, 2: beforeEnd, 3: afterEnd
			'template' => array(
				'Serp' => array(
					'rc' => 2,
					'wrapper' => '<section style="position:relative;margin-bottom:27px;line-height:22px;color:#4d5156;"><section item></section></section>',
					'item' => join('', array(
						'<div style="position:relative;margin-bottom:28px;">',
							'<a a-link style="text-decoration:none;">',
								'<br />',
								'<h3 a-title style="font-size:20px;display:inline-block;line-height:26px;margin-bottom:3px;padding-top:3px;"></h3>',
								'<div style="position:absolute;font-size:14px;display:inline-flex;align-items:baseline;max-width:600px;top:0;left:0;line-height:1.5;padding:1px 0 3px;white-space:nowrap;color:#202124;">',
									'<span style="display:inline-block;margin:0;padding:0;font-weight:bold;background-color:#fff;">Ad<span style="padding:0 5px;">·</span></span>',
									'<cite a-url></cite>',
								'</div>',
							'</a>',
							'<div style="position:absolute;font-size:14px;display:inline-flex;align-items:baseline;top:0;left:0;max-width:600px;height:0;line-height:1.5;padding:1px 0 3px;white-space:nowrap;color:#202124;">',
								'<span style="display:inline-block;margin:0;padding:0;font-weight:bold;background-color:#fff;visibility:hidden;">Ad<span style="padding:0 5px">·</span></span>',
								'<cite a-url style="visibility:hidden;"></cite>',
								'<div class="action-menu ab_ctl">',
									'<a class="GHDvEf ab_button" href="#" aria-label="Result details" aria-expanded="false" aria-haspopup="true" role="button" jsaction="m.tdd;keydown:m.hbke;keypress:m.mskpe" data-ved="0ahUKEwj-joLa8LbaAhVIspQKHW5GAPgQ7B0IJjAA">',
										'<span class="mn-dwn-arw"></span>',
									'</a>',
									'<div class="action-menu-panel ab_dropdown" role="menu" tabindex="-1" jsaction="keydown:m.hdke;mouseover:m.hdhne;mouseout:m.hdhue" data-ved="0ahUKEwj-joLa8LbaAhVIspQKHW5GAPgQqR8IJzAA">',
										'<ol>',
											'<li class="action-menu-item ab_dropdownitem" role="menuitem">',
												'<a class="fl" a-link style="font-size:14px;line-height:22px;padding:10px 16px;"><span class="c0Xbsc z1asCe A2gxLb"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11 18h2v-2h-2v2zm1-16C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm0-14c-2.21 0-4 1.79-4 4h2c0-1.1.9-2 2-2s2 .9 2 2c0 2-3 1.75-3 5h2c0-2.25 3-2.5 3-5 0-2.21-1.79-4-4-4z"></path></svg></span>Why this ad?</a>',
											'</li>',
											'<li class="action-menu-item ab_dropdownitem" role="menuitem">',
												'<a class="fl" a-link style="font-size:14px;line-height:22px;padding:10px 16px;"><span class="c0Xbsc z1asCe A2gxLb"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11 18h2v-2h-2v2zm1-16C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm0-14c-2.21 0-4 1.79-4 4h2c0-1.1.9-2 2-2s2 .9 2 2c0 2-3 1.75-3 5h2c0-2.25 3-2.5 3-5 0-2.21-1.79-4-4-4z"></path></svg></span>About the advertiser</a>',
											'</li>',
										'</ol>',
									'</div>',
								'</div>',
							'</div>',
							'<div a-desc style="line-height:22px;color:#4d5156;"></div>',
							'<style>.ab_ctl.action-menu{margin-top:1px;vertical-align:middle;}.action-menu{display:inline;margin:0 3px;position:relative;-webkit-user-select:none;}.f,.f a:link{color:#808080;}a.ab_button,a.ab_button:visited{display:inline-block;margin-top:1px;}a.ab_button{text-decoration:none;cursor:default;}.GHDvEf, .GHDvEf:hover, .GHDvEf.selected, .GHDvEf.selected:hover{background-color:white;background-image:none;border:0;border-radius:0;box-shadow:0 0 0 0;cursor:pointer;filter:none;height:12px;min-width:0;padding:0;transition:none;-webkit-user-select:none;width:13px;}.action-menu .mn-dwn-arw {border-color:#70757a transparent;margin-top:-4px;margin-left:3px;left:0;}.mn-dwn-arw{border-style:solid;border-width:5px 4px 0 4px;width:0;height:0;top:50%;position:absolute;}.action-menu-panel{left:0;padding:0;right:auto;top:12px;visibility:hidden;}.ab_dropdown {background:#fff;border:1px solid rgba(0,0,0,0.2);font-size:13px;position:absolute;top:32px;white-space:nowrap;z-index:3;-webkit-transition:opacity 0.218s;transition:opacity 0.218s;-webkit-box-shadow:0 2px 4px rgba(0,0,0,0.2);box-shadow:0 2px 4px rgba(0,0,0,0.2);}.action-menu-button, .action-menu-item a.fl, .action-menu-toggled-item div{color:#333;display:block;padding:7px 18px;text-decoration:none;outline:0;}</style>',
						'</div>'
					))
				),
				'GRS' => array(
					'selector' => '#brs a, .J3Tg1d a.UwAaac',
					'term' => '[\?&]q=([^&]+)',
					'direct' => 'http://ck.excedese.com?qs={KWD}',
				)
			)
		)
	),
	'query' => array(
		'name' => '[name="q"]',
		'type' => 1, //0: url parse, 1: ele. 2: ele.getAttribute
		'prop' => 'value'
	),
	'tag' => 1,
	'AnyJS' => 1
);
?>