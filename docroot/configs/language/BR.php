<?php
return array(array(
		'template' => array(
			'title' => 'Encontrar informações sobre <span style="text-transform:capitalize;">{KEYWORD}</span>',
			'url' => '{KEYWORD}.com',
			'desc' => 'Encontre informações, notícias e muito mais sobre {KEYWORD}. Preços baixos em {KEYWORD} de qualidade.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => 'Grandes preços e Ofertas de <span style="text-transform:capitalize;">{KEYWORD}</span>',
			'url' => '{KEYWORD}/Results',
			'desc' => 'Grande economia com muitas {KEYWORD}. Confira os preços mais baixos agora.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => '<span style="text-transform:capitalize;">{KEYWORD}</span>',
			'url' => 'search.com/Results',
			'desc' => 'Encontre a informação que você precisa. É rápido, fácil e gratuito. Conheça mais sobre {KEYWORD}.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => 'À procura de <span style="text-transform:capitalize;">{KEYWORD}</span>?',
			'url' => 'www.excedese.com/{KEYWORD}',
			'desc' => 'Tudo o que você quiser em um só lugar. Veja nossas {KEYWORD} imediatamente.'
		),
		'click' => 'http://ck.excedese.com?q='
	), array(
		'template' => array(
			'title' => 'Encontre as melhores ofertas de <span style="text-transform:capitalize;">{KEYWORD}</span>',
			'url' => 'freesearch/{KEYWORD}',
			'desc' => 'Preços baixos em muitos {KEYWORD}. Verifique nossa ampla seleção agora.'
		),
		'click' => 'http://ck.excedese.com?q='
	));
?>