<?php
$t_item = <<<ITEM
<li class="b_algo" style="padding:12px 20px 0;margin:0 0 2px;background-color:#fff;">
	<h2>
		<a target="_blank" a-link a-title></a>
	</h2>
	<div class="b_caption">
		<div class="b_attribute">
			<cite a-url></cite>
		</div>
		<p a-desc></p>
	</div>
</li>
ITEM;

$b_item = <<<ITEM
<li class="b_algo" style="padding:12px 20px 0;margin:0 0 2px;background-color:#fff;">
	<h2>
		<a target="_blank" a-link a-title></a>
	</h2>
	<div class="b_caption">
		<div class="b_attribute">
			<cite a-url></cite>
		</div>
		<p a-desc></p>
	</div>
</li>
ITEM;


return  array(
	'dir' => array(
		'type' => 0,	//0:form.action
		'selector' => '#sb_form [name="q"]',
		'sugselector' => '#sw_as .sa_drw',
		'attr' => 'query'
	),
	'slots' => array(
		't' => array(
			'selector' => '#b_results, .dg_b, #vm_res, #algocore',
			'type' => 1,
			'template' => array(
				'Serp' => array(
					'rc' => 3,
					'wrapper' => '<div><style>.algocore .b_algo{box-sizing:border-box;margin:0 16px 10px 0!important;padding:10px 10px 10px 20px!important;}</style><div item></div></div>',
					'item' => $t_item
				)
			)
		),
		'b' => array(
			'selector' => '#b_results > .b_ans:not(.b_top):not(.b_mop):not(.b_mopb)',
			'type' => 0, //0: beforeBegin, 1: afterBegin, 2: beforeEnd, 3: afterEnd
			'template' => array(
				'Serp' => array(
					'rc' => 2,
					'wrapper' => '<div><div item></div></div>',
					'item' => $b_item
				),
				'GRS' => array(
					'selector' => '.b_rs a',
					'term' => '[\?&]q=([^&]+)',
					'direct' => 'http://ck.excedese.com/web?qs={KWD}',
				)
			)
		)
	),
	'query' => array(
		'name' => '[name="q"]',
		'type' => 1, //0: url parse, 1: ele. 2: ele.getAttribute
		'prop' => 'value'
	)
);
?>