FROM php:5.6-apache

#RUN apt-get update && apt-get -y install cronolog

RUN apt-get update -y && \
    apt-get install -y cron vim cronolog && \
    touch /var/log/cron.log && \
    rm -rf /var/lib/apt/lists/*

RUN cp "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

COPY docroot/ /var/www/html/
COPY docker/apache/searchmomentumlite.conf /etc/apache2/sites-available/000-default.conf


RUN chown -R www-data:www-data /var/www/html \
    && a2enmod rewrite \
    && mkdir -p /mnt/apachelog/logs/pobijsservice/ \
    && chmod 777 -R /mnt/apachelog/logs/pobijsservice/ \
    && a2enmod remoteip

EXPOSE 80

CMD ["/usr/sbin/apache2ctl", "-DFOREGROUND"]