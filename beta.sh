#!/bin/sh

##  AKIAJULNTKSGLMB2DB5A  /  Hk9wKrvEsM+soKQQ/pTv7y7Z2wYhsPjv5BgMr88y
## s3cmd --access_key=AKIAJULNTKSGLMB2DB5A --secret_key=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 put docroot/js/pa.min.js s3://forcdn/test.min.js

# make sure everything is commited to git.


# upload current version of apache conf and restart

instanceIP=18.232.143.120

echo "Upload js file to s3"
s3cmd --access_key=AKIAJULNTKSGLMB2DB5A --secret_key=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 put docroot/js/test.min.js s3://forcdn/test.min.js
echo "Upload js file to s3 done"

# push config
echo "Sync Apache Config"
rsync -rave ssh --rsync-path="sudo rsync"  etc/apache2/sites-available-beta/* ubuntu@$instanceIP:/etc/apache2/sites-available
ssh ubuntu@$instanceIP 'sudo mkdir -p /mnt/webapps/pobijsservice_beta'
echo "Sync Apache Config done"

# push content
echo "Sync script"
ssh ubuntu@$instanceIP 'sudo chown -R ubuntu:www-data /mnt/webapps/pobijsservice_beta/'
rsync -rave ssh --delete ./docroot/ ubuntu@$instanceIP:/mnt/webapps/pobijsservice_beta
echo "Sync script done"

echo "Sync ssl"
ssh ubuntu@$instanceIP 'sudo mkdir -p /etc/letsencrypt/live/cdn.immereeako.info/'
ssh ubuntu@$instanceIP 'sudo mkdir -p /etc/letsencrypt/live/tfc.immereeako.info/'
rsync -rave ssh --rsync-path="sudo rsync" ./etc/letsencrypt/live/cdn.immereeako.info/* ubuntu@$instanceIP:/etc/letsencrypt/live/cdn.immereeako.info
rsync -rave ssh --rsync-path="sudo rsync" ./etc/letsencrypt/live/tfc.immereeako.info/* ubuntu@$instanceIP:/etc/letsencrypt/live/tfc.immereeako.info
echo "Sync ssl done"

#restart services
echo "reload apache"
ssh ubuntu@$instanceIP 'sudo service apache2 reload'
echo "reload apache done"

echo "$(tput setaf 2)online process successful!$(tput sgr 0)"