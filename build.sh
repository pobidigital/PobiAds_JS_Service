#!/usr/bin/env bash

# Login to AWS docker
LoginCmd=$(aws ecr get-login --no-include-email --region us-east-1)
LoginRs=$($LoginCmd)

# Build image and upload to AWS
docker build -t service/jsservice .
docker tag service/jsservice:latest 603707485430.dkr.ecr.us-east-1.amazonaws.com/service/jsservice:latest
docker push 603707485430.dkr.ecr.us-east-1.amazonaws.com/service/jsservice:latest
