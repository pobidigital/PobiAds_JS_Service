.PHONY: test release clean version login logout publish

export APP_VERSION ?= $(shell git rev-parse --short HEAD)

version:
	@ echo '{"Version": "$(APP_VERSION)"}'

login:
	aws --profile pbm ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 427114123052.dkr.ecr.us-east-1.amazonaws.com

logout:
	docker logout 427114123052.dkr.ecr.us-east-1.amazonaws.com

test:
	# docker-compose build --pull release
	docker-compose -f docker-compose-prod.yml build service_js_service
	# docker-compose run test

publish:
	docker-compose -f docker-compose-prod.yml push service_js_service
	# aws --profile pb1 s3 cp docroot/js/pa.min.js s3://forcdn/pa.min.js

clean:
	docker-compose down -v
	docker images -q -f dangling=true -f label=application=service_js_service | xargs -I ARGS docker rmi -f --no-prune ARGS%
