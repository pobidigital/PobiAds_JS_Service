if (window !== top || !(/^https?/.test(location.protocol))) {
	return;
}

var UNKNOW = 'unknow';

var env = require('./config/env'),
	ADCtrl = require('./modules/ADCtrl'),
	ready = require('./modules/ready'),
	paInfo = window['_paInfo_'] || {};

if (paInfo.sub1) {
	paInfo.sub1 = paInfo.sub1.toString().toLowerCase();
	paInfo.uid = paInfo.uid || UNKNOW;

	ready(function() {
		var adCtrl = new ADCtrl(env, paInfo);
	});
}