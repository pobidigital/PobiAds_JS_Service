(function(window, document) {
	if (!/\.google\./.test(location.hostname)) {
		return;
	}

	var template = [
		'<div class="g">',
			'<div>',
				'<div class="rc">',
					'<h3 class="r">',
						'<a href="https://www.google.com/search?q=Using%20Extensions%20In%20Chrome">Using <b>Extensions</b> In <b>Chrome</b></a>',
					'</h3>',
					'<div class="s">',
						'<div>',
							'<div class="f hJND5c TbwUpd" style="white-space:nowrap">',
								'<cite class="iUh30">www.shop411.com/Deals</cite>',
								'<div class="action-menu ab_ctl">',
									'<a class="GHDvEf ab_button" href="#" aria-label="Result details" aria-expanded="false" aria-haspopup="true" role="button" jsaction="m.tdd;keydown:m.hbke;keypress:m.mskpe" data-ved="0ahUKEwj-joLa8LbaAhVIspQKHW5GAPgQ7B0IJjAA">',
										'<span class="mn-dwn-arw"></span>',
									'</a>',
									'<div class="action-menu-panel ab_dropdown" role="menu" tabindex="-1" jsaction="keydown:m.hdke;mouseover:m.hdhne;mouseout:m.hdhue" data-ved="0ahUKEwj-joLa8LbaAhVIspQKHW5GAPgQqR8IJzAA">',
										'<ol>',
											'<li class="action-menu-item ab_dropdownitem" role="menuitem">',
												'<a class="fl" href="https://webcache.googleusercontent.com/search?q=cache:fyycimqOxJoJ:https://en.wikipedia.org/wiki/DVD+&amp;cd=1&amp;hl=en&amp;ct=clnk" ping="/url?sa=t&amp;source=web&amp;rct=j&amp;url=https://webcache.googleusercontent.com/search%3Fq%3Dcache:fyycimqOxJoJ:https://en.wikipedia.org/wiki/DVD%2B%26cd%3D1%26hl%3Den%26ct%3Dclnk&amp;ved=0ahUKEwj-joLa8LbaAhVIspQKHW5GAPgQIAgoMAA">Cached</a>',
											'</li>',
											'<li class="action-menu-item ab_dropdownitem" role="menuitem">',
												'<a class="fl" href="/search?dcr=0&amp;q=related:https://en.wikipedia.org/wiki/DVD+dvd&amp;tbo=1&amp;sa=X&amp;ved=0ahUKEwj-joLa8LbaAhVIspQKHW5GAPgQHwgpMAA">Similar</a>',
											'</li>',
										'</ol>',
									'</div>',
								'</div>',
							'</div>',
							'<span class="st">Find our Lowest Possible Price! Using <b>Extensions</b> In <b>Chrome</b> for Sale</span>',
						'</div>',
					'</div>',
				'</div>',
			'</div>',
		'</div>'
	];
	
	var container = document.getElementById('center_col'),
		adContainer = document.createElement('div');

	adContainer.innerHTML = template.join('');
	adContainer.className = 'med';
	adContainer.style.padding = '0 16px';
	container.insertAdjacentElement('afterBegin', adContainer);
}) (window, document);