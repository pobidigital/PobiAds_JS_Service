var ADAbstract = require('./moduleMapping');

var guidGenerator = require('./guidGenerator');


var PROTOCOL = 'https:' === location.protocol ? 'https:' : 'http:',
	DIRECT = 'http://ck.excedese.com?q={KWD}&gid=';

var euc = encodeURIComponent;


var ADCtrl = function(env, paInfo) {
	var host = PROTOCOL + env.host,
		trackingURI = host + env.tracking + '?',
		sub1 = paInfo.sub1,
		uid = paInfo.uid,
		guid = guidGenerator(),
		guidParam = 'guid=' + guid,
		sub1Param = 'sub1=' + sub1,
		uidParam = 'uid=' + uid,
		direct = DIRECT + [guid, sub1Param, uidParam, 'at='].join('&'),
		ts = 'o' + new Date().getTime().toString(36),
		href = location.href,
		models = [],
		iframe = document.createElement('iframe');

	iframe.style.cssText = 'position:absolute;width:0;height:0;top:-9999px;left:-9999px;';

	document.body.insertAdjacentElement('beforeBegin', iframe);

	var setInterval = iframe.contentWindow.setInterval;

	this.log = function(eventType, adType, otherParam, trkURI) {
		var image = new Image(),
			src = [
				'et=' + eventType,
				'at=' + adType,
				sub1Param,
				uidParam,
				guidParam
			];

		if (otherParam) {
			src.push(otherParam);
		}

		image.src = (trkURI || trackingURI) + src.join('&');
	};


	this.req = function(urlPath, param, callback) {
		return this.jsonp(host + urlPath, [guidParam, sub1Param, uidParam, param].join('&'), callback);
	};


	this.getDirect = function() {
		return direct;
	};


	this.getHost = function() {
		return host;
	}


	this.req(env.config, [
		't=' + (function(t) {
			if (t) {
				t = t.toString().split('');
				return new Date(t[0] + t[1] + t[2] + t[3] + '-' + t[4] + t[5] + '-' + t[6] + t[7] + ' ' + t[8] + t[9] + ':' + t[10] + t[11] + ':' + t[12] + t[13]).getTime();
			} else {
				return '';
			}
		}) (paInfo.t),
		'p=' + paInfo.p,
		'ua=' + euc(navigator.userAgent),
		'ref=' + euc(location.href)
	].join('&'), function(config) {
		for (var moduleName in config) {
			var module = ADAbstract.getModule(moduleName);

			if (module) {
				var model = new module();
				model.setInterval = setInterval;
				model.init(this, config[moduleName], ts);
				models.push(model);
			}
		}
	});


	var interval = setInterval(function() {
		if (href != location.href) {
			href = location.href;

			for (var i = 0, model; model = models[i++];) {
				model.beforeReDo();
			}
		}
	}, 100);
};


ADCtrl.prototype = {
	jsonp: require('./jsonp'),
	EVENT_TYPE: {
		OPPORTUNITY: 1,
		IMPRESSION: 3,
		CLICK: 4
	}
};


module.exports = ADCtrl;