var ADAbstract = require('./ADAbstract');

ADAbstract.setModuleMapping({
	Search: require('./SearchCtrl'),
	Serp: require('./Serp'),
	GRS: require('./GRS'),
	Pop: require('./Pop'),
	TagRV: require('./TagRV'),
	TagMZ: require('./TagMZ'),
	ModuleApi: require('./ModuleApi'),
	Intext: require('./Intext'),
	RS: require('./RS'),
	Banner: require('./Banner'),
	AnyJS: require('./AnyJS')
});

module.exports = ADAbstract;