var ADAbstract = require('./ADAbstract');

var Serp = function() {};

var prototype = Serp.prototype = new ADAbstract(Serp);


Serp.AT = 1;
Serp.PATH = 's';
Serp.BIT = 1;


Serp.getOpts = function(models, kwd) {
	var maxRc = models[0].rc | 0;

	for (var i = 1, model; model = models[i++];) {
		//maxRc = Math.max(maxRc, model.rc | 0);
		maxRc += (model.rc | 0);
	}

	return ['rc=' + maxRc, function(config) {
		for (var i = 0, model; model = models[i++];) {
			//model.cb(config);
			model.cb(config.splice(0, model.rc));
		}
	}];
};


prototype.set = function(ctrl, config, ts, id) {
	this.rc = config.rc;
	this.id = id;
	this.idParam = 'p=' + id;
};


prototype.getFlag = function(ctrl, config, div, kwd) {
	this.div = div;
	return Serp.BIT;
};


prototype.render = function(data) {
	return this.ctrl.render(this.config, data, this.div, this.rc);
};


prototype.getOtherParams = function(config) {
	return ['n=' + Math.min(config.length, this.rc), this.idParam].join('&');
};


module.exports = Serp;