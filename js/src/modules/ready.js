var t = document,
	flag = /^(loaded|complete)$/.test(t.readyState),
	readyFnList = [];

!flag && t.addEventListener('readystatechange', function() {
	if (flag = /^(loaded|complete)$/.test(this.readyState)) {
		for (var t; t = readyFnList.shift();) {
			t.apply(this, arguments);
		}
	}
});

module.exports = function(readyFn) {
	flag ? readyFn.call(t) : readyFnList.push(readyFn);
};