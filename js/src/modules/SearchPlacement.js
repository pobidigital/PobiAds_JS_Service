var INSERT_TYPE = require('./InsertType');

var DIV = 'div',
	A_ = 'a-',
	A_LINK = A_ + 'link',
	A_LINK_SELECTOR = 'a[' + A_LINK + ']',
	ITEM = 'item',
	ITEM_SELECTOR = '[' + ITEM + ']',
	REMOVE_ATTRIBUTE = 'removeAttribute',
	CHILDREN = 'children';

var ATTR_ARRAY = ['[', '', ']']
	EMPTY_STR = '';


var ADInter = require('./ADInter'),
	ADAbstract = require('./ADAbstract'),
	create = require('./create');


var body = document.body;


var SearchPlacement = function() {};


var prototype = SearchPlacement.prototype = new ADInter();


prototype.afterInit = function(ctrl, slot, ts, slotKey, kwd) {
	var selector = slot.selector,
		insertType = INSERT_TYPE[slot.type],
		template = slot.template,
		searchContainer = create(DIV),
		models = [],
		container;

	searchContainer.setAttribute(ts, EMPTY_STR);

	this.reDo = function(kwd_) {
		var bin = 0,
			result = {
				models: {}
			},
			models_ = result.models;

		kwd = kwd_;
		searchContainer.innerHTML = EMPTY_STR;

		for (var i = 0, model; model = models[i++];) {
			var div = create(DIV),
				bit = model.getFlag(this, model.config, div, kwd_);

			searchContainer.appendChild(div);

			if (bit) {
				bin |= bit;
				models_[bit] = model;
			}
		}

		result.bin = bin;

		return result;
	};

	this.insert = function() {
		if (!searchContainer) {
			return;
		}

		/*if (!container) {
			container = body.querySelector(selector);
		}*/

		container = document.body.querySelector(selector);

		if (container && !container.parentNode.contains(searchContainer)) {
			container.insertAdjacentElement(insertType, searchContainer);
		}
	};

	this.clear = function() {
		searchContainer.innerHTML = EMPTY_STR;
	};

	this.render = function(config, data, adContainer, len) {
		var div = create(DIV),
			item = config.item;

		len = len || data.length;
		div.innerHTML = config.wrapper;

		var itemContainer = div.querySelector(ITEM_SELECTOR);

		if (itemContainer) {
			itemContainer[REMOVE_ATTRIBUTE](ITEM);

			for (var i = 0; i < len; i++) {
				var row = data[i];

				if (!row) {
					return;
				}

				var template = row.template,
					click = row.click + '&p=' + slotKey,
					temp = create(DIV);

				temp.innerHTML = item;

				for (var key in template) {
					var attrName = ATTR_ARRAY[1] = A_ + key,
						eleList = temp.querySelectorAll(ATTR_ARRAY.join(EMPTY_STR));

					for (var j = 0, ele; ele = eleList[j++];) {
						ele.innerHTML = template[key];
						ele[REMOVE_ATTRIBUTE](attrName);
					}
				}

				var linkList = temp.querySelectorAll(A_LINK_SELECTOR);

				for (var j = 0, link; link = linkList[j++];) {
					link.href = click;
					link[REMOVE_ATTRIBUTE](A_LINK);
				}

				itemContainer.appendChild(temp[CHILDREN][0]);
			}

			adContainer.appendChild(div[CHILDREN][0]);

			return true;
		}
	};

	var reg = /\{ID\}/ig;


	for (var moduleKey in template) {
		var templateObj = template[moduleKey],
			module = ADAbstract.getModule(moduleKey),
			model = new module(),
			wrapper;

		if (templateObj && (wrapper = templateObj.wrapper)) {
			templateObj.wrapper = wrapper.replace(reg, ts);
		}

		model.init(this, templateObj, ts, slotKey);
		models.push(model);
	}
};


module.exports = SearchPlacement;