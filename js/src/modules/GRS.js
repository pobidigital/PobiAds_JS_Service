var ADAbstract = require('./ADAbstract');

var KWD = '{KWD}',
	EMPTY_S = '';

var GRS = function() {};

var prototype = GRS.prototype = new ADAbstract(GRS);


GRS.AT = 2;
GRS.BIT = 2;


prototype.set = function(ctrl, config, ts, id) {
	this.id = id;
	this.idParam = 'p=' + id;
};


prototype.getFlag = function(ctrl, config, div, kwd) {
	//this.div = div;

	var selector = config.selector,
		body = document.body,
		me = this;

	clearInterval(this.interval);

	this.interval = setInterval(function() {
		var aList = body.querySelectorAll(selector);

		if (aList.length > 0) {
			var reg = new RegExp(config.term, 'i'),
				count = 0,
				ts = me.ts,
				direct = me.getDirect() + GRS.AT;

			clearInterval(me.interval);

			for (var i = 0, a; a = aList[i++];) {
				var regResults = reg.exec(a.href);

				if (regResults) {
					a.href = direct.replace(KWD, regResults[1]);
					a.setAttribute(ts, EMPTY_S);
					count ++;
				}
			}

			me.imp({
				count: count
			});
		}
	}, 100);

	return 0;
};


prototype.getOtherParams = function(config) {
	return ['n=' + config.count, this.idParam].join('&');
};


module.exports = GRS;