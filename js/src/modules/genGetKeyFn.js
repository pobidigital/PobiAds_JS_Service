var MAPPING = [
	function(name) {
		return function() {
			var reg = new RegExp('[\?&#]' + name + '=([^\&#]*)','g'),
				results = reg.exec(location.href);

			return results && decodeURIComponent(results[1]);
		}
	},

	function(name, prop) {
		return function() {
			var ele = document.querySelector(name);
			return ele[prop];
		}
	}, 

	function(name, prop) {
		return function() {
			var ele = document.querySelector(name);
			return ele.getAttribute(prop);
		}
	}
];


module.exports = function(queryObj) {
	return MAPPING[queryObj.type](queryObj.name, queryObj.prop);
};