var ADAbstract = require('./ADAbstract');

var encodeURI = encodeURIComponent;

var Intext = function() {};

var prototype = Intext.prototype = new ADAbstract(Intext);

Intext.AT = 7;
Intext.BIT = 5;


prototype.set = function(ctrl, config, ts) {
	var eles = document.body.querySelectorAll(config.slots),
		textNodes = [];

		findTextNode(eles, textNodes);
		analysis(textNodes, config.kwdReg, config.url);
		this.imp();
};


function findTextNode(eles, textNodes) {
	for (var i = 0, ele; ele = eles[i++];) {
		var childNodes = ele.childNodes;

		for (var j = 0, child; child = childNodes[j++];) {
			var nodeName = child.nodeName;

			if ('#text' === nodeName) {
				textNodes.push(child);
			} else if ('A' === nodeName) {
				continue;
			} else {
				var childList = ele.childNodes;

				if (childList) {
					findTextNode(childList, textNodes);
				}
			}
		}
	}

	return textNodes;
}


function analysis(textNodes, kwdReg, url) {
	for (var i = 0, textNode; textNode = textNodes[i++];) {
		var parent = textNode.parentNode,
			textContent = textNode.textContent,
			matchResults = textContent.match(kwdReg);
		
		if (matchResults) {
			var matchResult = matchResults[1],
				aLink = document.createElement('a');

			textNode = textNode.splitText(matchResults.index);
			textNode.splitText(matchResult.length);

			aLink.innerHTML = matchResult;
			aLink.href = url + encodeURIComponent(matchResult);
			aLink.target = '_blank';
			textNode.parentNode.replaceChild(aLink, textNode);
		}
	}
}


module.exports = Intext;