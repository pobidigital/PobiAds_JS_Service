var INSERT_TYPE = require('./InsertType');

var ADAbstract = require('./ADAbstract');

var RS = function() {};

var prototype = RS.prototype = new ADAbstract(RS);

RS.AT = 8;
RS.BIT = 6;

var iframe;


prototype.set = function(ctrl, config, ts) {
	var self = this;

	this.interval = setInterval(function() {
		if (iframe && !document.body.contains(iframe)) {
			self.render(ctrl, config, ts);
		}
	}, 100);

	this.render(ctrl, config, ts);
};


prototype.render = function(ctrl, config, ts) {
	if (!location.pathname || !location.search) {
		return;
	}

	var BORDER_COLOR = '#d4d4d4';

	var keywords = config.keywords,
		url = config.url,
		iframeCss = ['position:fixed', 'display:block', 'visibility:visible', 'top:auto', 'right:0', 'bottom:0', 'left:0', 'width:100%', 'height:94px', 'transform:translate(0,0)', 'opacity:1', 'border:none', 'outline:0', 'z-index:2147483647', ''];

	iframe = document.createElement('iframe');
	iframe.style.cssText = iframeCss.join('!important;');
	iframe.setAttribute(ts, ts);

	document.body.appendChild(iframe);

	var iframeDocument = iframe.contentWindow.document;

	iframeDocument.write([
		'<!DOCTYPE html>',
		'<html>',
		'<head>',
			'<style>*,*:before,*:after{box-sizing:border-box;}div{height:100%;}.main:before{content:"Ads";position:absolute;display:block;top:-11px;left:32px;padding:2px 6px 0;line-height:14px;border:1px solid ' + BORDER_COLOR + ';border-bottom:0;border-radius: 4px 4px 0 0;font-size:8px;font-weight:400;z-index:1;color:#202526;background:#fff;}.close{position:relative;width:16px;height:16px;cursor:pointer;}.close:before,.close:after{content:"";position:absolute;display:block;top:0;right:0;bottom:0;left:0;width:2px;height:141.42135623730951%;margin:auto;background:#fff;}.close:before{transform:rotate(45deg);}.close:after{transform:rotate(-45deg);}.center{display:flex;align-items:center;justify-content:center;}.right:before,.left:before{content:"";border:8px solid transparent;}.right:before{border-left:10px solid ' + BORDER_COLOR + ';border-right:0;}.left:before{border-right:10px solid ' + BORDER_COLOR + ';border-left:0;}.ad-section{flex:1 0 300px;margin-right:-.5px;padding:8px 8px 8px 32px;}.ad-section+.ad-section{border-left:1px solid ' + BORDER_COLOR + ';}.ad-section a{display:flex;align-items:center;height:100%;font-size:16px;font-weight:500;color:inherit;word-break:break-word;text-transform:capitalize;}</style>',
		'</head>',
		'<body>',
			'<div id="' + ts + '" style="position:absolute;top:0;left:0;width:100%;transform:translate(0,100%);' + ['-webkit-', '-moz-', '-ms-', '-o-', '', ''].join('transition:opacity .5s linear,transform .5s cubic-bezier(.175,.885,.32,1.275);') + '">',
				'<div class="main" style="position:absolute;display:flex;bottom:0;left:0;width:100%;height:80px;' + ['-webkit-', '-moz-', '', ''].join('box-shadow:0 0 12px rgba(0,0,0,.1);') + 'background:#fff;">',
					'<div class="center" style="width:32px;background:#42b983;">',
						'<div id="close" class="close"></div>',
					'</div>',
					'<div id="ad-area" style="display:flex;flex:1;border:1px solid ' + BORDER_COLOR + ';border-right:0;overflow:hidden;">',
						// '<div id="ad-wrapper" style="display:flex;">',
						(function(keywords, url) {
							var STRING = 'string';
							var list = [];

							for (var i = 0, keyword; keyword = keywords[i++];) {
								if (STRING === typeof keyword) {
									list.push('<div class="ad-section"><a target="_blank" href="' + url + encodeURIComponent(keyword) + '"><span>' + keyword + '</span></a></div>');
								}
							}

							return list.join('');
						}) (keywords, url),
						// '</div>',
					'</div>',
					'<div class="arrow" style="width:33px;border:1px solid ' + BORDER_COLOR + ';border-right:0;box-shadow:0 0 12px rgba(0,0,0,0.1);">',
						'<div id="right" class="center right" dir="0" style="height:40.5px;border-bottom:1px solid ' + BORDER_COLOR + ';"></div>',
						'<div id="left" class="center left" dir="-1" style="height:39.5px;"></div>',
					'</div>',
				'</div>',
			'</div>',
			'<script>',
				'setTimeout(function(){document.getElementById("' + ts + '").style.transform="translate(0,0)";},200);',
			'</script>',
		'</body>',
		'</html>'
	].join(''));

	var adContainer = iframeDocument.getElementById(ts),
		close = iframeDocument.getElementById('close'),
		right = iframeDocument.getElementById('right'),
		adArea = iframeDocument.getElementById('ad-area'),
		left = iframeDocument.getElementById('left'),
		status = 1,
		self = this,
		interval;

	var STEP = 20,
		N = 15;

	close.addEventListener('click', function() {
		adContainer.style.transform = 'translate(0, 100%)';

		setTimeout(function() {
			iframe.parentNode.removeChild(iframe);
			clearInterval(self.interval);
		}, 550);
	});

	right.addEventListener('click', scrollHandler);

	left.addEventListener('click', scrollHandler);


	function scrollHandler() {
		if (status) {
			var dir = this.getAttribute('dir'),
				width = adArea.offsetWidth - 1,
				scrollWidth = adArea.scrollWidth - width,
				scrollLeft = adArea.scrollLeft,
				dirTarget = (dir ^ -1) & scrollWidth,
				targetNum = scrollLeft === dirTarget ? (dirTarget ^ scrollWidth) : (scrollLeft + ((dir | 1) * width)),
				n = N,
				average = (targetNum - scrollLeft) / n;

			interval = setInterval(function() {
				n --;
				
				adArea.scrollLeft += average;

				if (!n) {
					status = 1;
					clearInterval(interval);
				}
			}, STEP);

			status = 0;
		}
	}

	this.imp();
};


module.exports = RS;