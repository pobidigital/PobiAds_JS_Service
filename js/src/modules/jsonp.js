var CB = 'cb',
	HEAD = document.getElementsByTagName('head')[0],
	ADD_EVENT_LISTENER = 'addEventListener',
	REMOVE_EVENT_LISTENER = 'removeEventListener',
	LOAD = 'load',
	ERROR = 'error';

var append = HEAD.appendChild;

if (window.Element && window.Node) {
	try {
		if (Element.prototype.appendChild != Node.prototype.appendChild) {
			append = Node.prototype.appendChild;
		}
	} catch (e) {}
}


module.exports = function(host, query, callback, cbParamName, isEncrypt) {
	var script = document.createElement('script'),
		randomFnName = 'pacb_' + (new Date().getTime().toString(36)) + ((Math.random() * 1e7) | 0).toString(36),
		status = 1,
		me = this;

	script.type = 'text/javascript';
	script.src = host + '?' + (cbParamName || CB) + '=' + randomFnName + '&' + query;

	script[ADD_EVENT_LISTENER](LOAD, remove, true);
	script[ADD_EVENT_LISTENER](ERROR, remove, true);


	window[randomFnName] = function() {
		if (status) {
			callback && callback.apply(me, arguments);
		}
	}

	append.call(HEAD, script);
	

	function remove() {
		HEAD.contains(this) && HEAD.removeChild(this);
		delete window[randomFnName];
		this[REMOVE_EVENT_LISTENER](LOAD, remove, true);
		this[REMOVE_EVENT_LISTENER](ERROR, remove, true);
		remove = null;
	}

	return {
		abort: function() {
			status = 0;
		}
	}
};