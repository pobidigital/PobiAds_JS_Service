var ADInter = require('./ADInter'),
	SearchPlacement = require('./SearchPlacement'),
	genGetKeyFn = require('./genGetKeyFn');


var SearchCtrl = function() {};


var prototype = SearchCtrl.prototype = new ADInter();


prototype.afterInit = function(ctrl, config, ts) {
	var queryObj = config.query,
		slots = config.slots,
		getKeyFn = genGetKeyFn(queryObj),
		kwd = getKeyFn(),
		placements = [],
		jsonps = [],
		setInterval = this.setInterval,
		sListener = queryObj.sListener,
		dir = config.dir,
		me = this;

	if (dir) {
		try {
			var inputEle = document.body.querySelector(dir.selector);

			if (inputEle) {
				var form = inputEle.form;

				if (form) {
					var sugselector = dir.sugselector,
						sugLayer = document.querySelector(sugselector),
						url = dir.url,
						attr = dir.attr,
						target = dir.target,
						reg = new RegExp('([\\?&]' + inputEle.name + '=)([^&]*)');

					form.onsubmit = null;
					form.removeAttribute('onsubmit');

					target && form.setAttribute('target', target);

					form.addEventListener('submit', function(event) {
						window.open(url + encodeURIComponent(inputEle.value), '_blank');
					});

					document.addEventListener('click', function(event) {
						var target = event.target;

						if (!sugLayer) {
							sugLayer = document.querySelector(sugselector);
						}

						if (sugLayer && sugLayer.contains(target)) {
							do {console.log(target);
								if (target.hasAttribute(attr)) {
									window.open(url + encodeURIComponent(target.getAttribute(attr) || target.innerText.replace(/[\n\r]+/, '')), '_blank');
									break;
								}
							} while (document !== (target = target.parentNode))

							event.stopPropagation();
							event.preventDefault();
						}
					}, true);
				}
			}
		} catch (e) {console.log(e);}
	}

	this.reDo = function() {
		if (kwd) {
			var bin = 0,
				results = [];

			for (var i = 0, jsonp; jsonp = jsonps[i++];) {
				jsonp.abort();
			}

			jsonps = [];

			for (var i = 0, placement; placement = placements[i++];) {
				var result = placement.reDo(kwd);
				bin |= result.bin;
				results.push(result);
			}

			this.load(bin, results);
		} else {
			for (var i = 0, placement; placement = placements[i++];) {
				placement.clear();
			}
		}
	};

	this.load = function(bin, results) {
		var modelMapping = {};

		for (var i = 0, result; result = results[i++];) {
			var models = result.models;

			for (var bit in models) {
				if (!modelMapping[bit]) {
					modelMapping[bit] = [];
				}

				modelMapping[bit].push(models[bit]);
			}
		}

		while (bin) {
			var bit = -bin & bin,
				models = modelMapping[bit],
				module = models[0].M,
				opts = module.getOpts(models, kwd);

			bin ^= bit;
			jsonps.push(this.req(module.PATH, opts[0] + '&q=' + encodeURIComponent(kwd), opts[1]));
		}
	};

	this.beforeReDo = function() {
		var kwd_ = getKeyFn();

		if (kwd_ !== kwd) {
			kwd = kwd_;
			this.reDo();
		}
	};

	var interval = setInterval(function() {
		for (var i = 0, placement; placement = placements[i++];) {
			placement.insert();
		}
	}, 50);


	for (var slotKey in slots) {
		var slot = slots[slotKey],
			placement = new SearchPlacement();

		placement.init(this, slot, ts, slotKey, kwd);
		placements.push(placement);
	}

	this.reDo();

	if (sListener) {
		var listenerEle = document.body.querySelector(sListener);

		if (listenerEle) {
			listenerEle.addEventListener('DOMNodeInserted', function() {
				me.beforeReDo();
			});
		}
	}
};


prototype.jsonp = require('./jsonp');


module.exports = SearchCtrl;