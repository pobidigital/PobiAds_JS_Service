var ADInter = function() {};


ADInter.prototype = {
	init: function(ctrl, config, ts) {
		this.ctrl = ctrl;
		this.ts = ts;
		this.config = config;
		this.log = ctrl.log;
		this.req = ctrl.req;
		this.getDirect = ctrl.getDirect;
		this.jsonp = ctrl.jsonp;
		this.EVENT_TYPE = ctrl.EVENT_TYPE;
		this.getHost = ctrl.getHost;

		return this.afterInit.apply(this, arguments);
	},
	afterInit: function(ctrl, config, ts) {},
	getFlag: function() {//Interface: before request
		return true;
	},
	beforeReDo: function() {},
	reDo: function() {}, //Interface: for secord search
	cb: function(config) {}//Interface: after request callback
};


module.exports = ADInter;