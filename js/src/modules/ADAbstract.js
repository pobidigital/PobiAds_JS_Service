var MODULE_MAPPING;


var ADInter = require('./ADInter');


var ADAbstract = function(module) {
	Object.defineProperty(this, 'M', {
		value: module
	});
};


ADAbstract.getModule = function(key) {
	return MODULE_MAPPING[key];
};


ADAbstract.setModuleMapping = function(moduleMapping) {
	MODULE_MAPPING = moduleMapping;
};


var prototype = ADAbstract.prototype = new ADInter();


prototype.AT = 0;


prototype.afterInit = function(ctrl, config) {
	return this.set.apply(this, arguments);
};

prototype.set = function() {};//Interface: before request


prototype.getOtherParams = function() {//Interface: get other log params
	return '';
};


prototype.cb = function(config) {
	this.render(config) && this.imp(config);
};

prototype.imp = function(config) {
	this.log(this.EVENT_TYPE.IMPRESSION, this.M.AT, this.getOtherParams(config));
};

prototype.setCookie = function(key, value) {

};

prototype.getCookie = function(key) {
	var reg = new RegExp('(^| )' + key + '=([^;]*)(;|$)'),
		results = document.cookie.match(reg);

	return results && results[2] || null;
}


//prototype.autoRequest = true;


module.exports = ADAbstract;