var ADAbstract = require('./ADAbstract');

var ModuleApi = function() {};

var prototype = ModuleApi.prototype = new ADAbstract(ModuleApi);


ModuleApi.AT = 6;
ModuleApi.BIT = 4;


prototype.set = function(ctrl, config, ts) {
	var script = document.createElement('script');
	script.src = config.js;
	document.body.appendChild(script);

	this.imp();
};


prototype.cb = function(data) {

};


module.exports = ModuleApi;