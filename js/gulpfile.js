const ENV_PATH = './src/config/env.js';
//const JS_PATH = 'dist/';
const JS_PATH = '../docroot/js/';
const ENV_PARAMETER = '-e';
const ARG_MAPPING = {};


ARG_MAPPING[ENV_PARAMETER] = 'dev';


FILE_NAME_MAPPING = {
	dev:	'dev.min.js',
	qa:		'test.min.ja',
	pro:	'pa.min.js',
	adv:	'pa.min.js'
}

var env;


var gulp = require('gulp'),
	browserify = require('browserify'),
	source = require('vinyl-source-stream'),
	buffer = require('gulp-buffer'),
	uglify = require('gulp-uglify'),
	fs = require('fs');


gulp.task('set-parameter', function() {
	var reg = /^\-/;
	argvs = process.argv.slice(2);

	for (var i = 0, argv; argv = argvs[i++];) {
		if (reg.test(argv)) {
			var parameter = argvs[i];
			ARG_MAPPING[argv] = !reg.test(parameter) && (i++, parameter) || ARG_MAPPING[argv];
		}
	}
});


gulp.task('build-environment', function() {
	env = ARG_MAPPING[ENV_PARAMETER];

	FILE_NAME_MAPPING[env]
		? fs.writeFileSync(ENV_PATH, fs.readFileSync(ENV_PATH + '.' + env))
		: (console.log(['[Error]: The Parameter must be ', ' dev or ', ' pro or ', ' qa or ', ' adv, for example: gulp', ' qa'].join(ENV_PARAMETER)), process.exit(1));
});


gulp.task('build-js', function() {
	return browserify({
		entries:['./src/main.js']
	})
	.bundle().on('error', function(e) {
		console.log(e);
	})
	.pipe(source(FILE_NAME_MAPPING[env]))
	.pipe(buffer())
	.pipe(uglify())
	.pipe(gulp.dest(JS_PATH))
	.on('finish', function() {

	});
});


gulp.task('default', ['set-parameter', 'build-environment', 'build-js'], function() {

});